@extends('layouts.app')
@section('title') Admin Dashboard @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="display: flex;">
            <div class="col-md-6">
            <a href="{{route('Categories.index')}}" class="btn btn-primary">Categories</a>
            </div>
            <div class="col-md-6 end-align">
            <a href="{{route('Categories.nested')}}" class="btn btn-warning">Nested Categories</a>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admin Dashboard') }}</div>

                <div class="card-body">

                    <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Is Admin</th>
                            <th>Options</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name}} </td>
                                <td>{{ $user->email}}</td>
                                <td><input type="checkbox" {{ ($user->is_admin)?'checked':''}}/></td>
                                @if(!$user->is_admin)
                                <td><a class="btn btn-danger" href="{{route('user.show',$user->id)}}">Dashboard</a></td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
            
        
    </div>
</div>
@endsection
