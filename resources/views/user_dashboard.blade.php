@extends('layouts.app')
@section('title') User Dashboard @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="display: flex;">
            <div class="col-md-6">
            <a href="{{route('Categories.index')}}" class="btn btn-primary">Categories</a>
            </div>
            <div class="col-md-6 end-align">
            <a href="{{route('Categories.nested')}}" class="btn btn-warning">Nested Categories</a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('User Dashboard') }}
                
            </div>
                <div class="card-body">
                   Hi {{ $user->name}}, you are logged in!.  
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
