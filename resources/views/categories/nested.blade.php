@extends('layouts.app')
@section('title') Nested Categories @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-2">
           <a href="{{route('home')}}" class="btn btn-danger">Home</a>
        </div>
        </div>
        <div class="col-md-8">
            <div class="card">
             
			@foreach($categories as $category)
			<ul>
			  <li>{{$category->name}}</li>
			  @if($category->sub_categories)
			  @foreach($category->sub_categories as $subcategory)
			  <ul>
			  	<li>{{$subcategory->name}}</li>
			  </ul>
			  @endforeach
			  @endif
			</ul> 
			@endforeach

               
            </div>
        </div>

        
    </div>
</div>
@endsection



