@extends('layouts.app')
@section('title') Edit Categories @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12"  style="display: flex;">
            <div class="col-md-6">
            <a href="{{route('Categories.index')}}" class="btn btn-primary">All Categories</a>
            </div>
            <div class="col-md-3 center-align">
            <a href="{{route('Categories.nested')}}" class="btn btn-warning">Nested Categories</a>
            </div>
            <div class="col-md-3 end-align">
            <a href="{{route('home')}}" class="btn btn-danger">Home</a>
            </div>
        </div>
        <div class="col-md-6">
          
             
			<form action="{{route('Categories.update',$category->id)}}" method="post">
				@csrf
				<div class="form-group">
				    <label for="category">Category:</label>
				    <input type="text" class="form-control" id="category" name="name" value="{{$category->name}}">
			    </div>
			    <br>
		  		<div class="form-group">
		    		<label for="pwd">Choose Parent Category:</label>
		    		<select name="parent_id" class="form-control">
		    			<option value="">Choose Category</option>
		    			@foreach($categories as $cat)
		    			<option value="{{$cat->id}}" {{($cat->id == $category->parent_id)?'selected':''}}>{{$cat->name}}</option>
		    			@endforeach
		    		</select>
		  		</div>
		  		<br>
		  		<button type="submit" class="btn btn-primary">Submit</button>
			</form>

               
            
        </div>

        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

        
    </div>
</div>
@endsection



