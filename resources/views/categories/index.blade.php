@extends('layouts.app')
@section('title') All Categories @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="display:flex">
        	
            <div class="col-md-6">
            @if(auth()->user()->is_admin)
            <a href="{{route('Create.categories')}}" class="btn btn-primary">Add Categories</a>
            @endif
            </div>
            
            <div class="col-md-3 center-align">
            <a href="{{route('Categories.nested')}}" class="btn btn-warning">Nested Categories</a>
            </div>
            <div class="col-md-3 end-align">
            <a href="{{route('home')}}" class="btn btn-danger">Home</a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">

			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td>Sr. No</td>
						<td>Category</td>
						<td>Parent Category</td>
						<td>Options</td>
					</tr>
				</thead>
				<tbody>
					@foreach($categories as $key=>$category)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$category->name}}</td>
						@if(!empty($category->parent_id) )
						@php $parent_cat = \App\Models\Category::where('id',$category->parent_id)->first(); @endphp
						<td>{{$parent_cat->name}}</td>
						 @else
						 <td>-</td>
						 @endif
						
						<td>
							@if(auth()->user()->is_admin)
							<a href="{{route('Categories.edit',$category->id)}}">Edit</a>
							<a href="#" class="deleteCategory" data-id="{{$category->id}}">Delete</a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

               
            </div>
        </div>

        
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	
	$(".deleteCategory").click(function(event){
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    $.ajax({
        url: "delete-category/"+id,
        type: 'post',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (data){
           if(data.success){
           window.location.reload();
           }
        },
        error: function(event){
        	console.log(event);
        }
    });
   
});
</script>
@endsection



