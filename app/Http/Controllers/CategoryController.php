<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    #================ Show all categories with childer categories ===========
    public function index()
    {
        $categories = Category::with('sub_categories')->get();

        return view('categories.index', compact('categories'));
    }


    #================ Create Category  ============================
    public function create()
    {
        $categories = Category::whereNull('parent_id')->get();

        return view('categories.create', compact('categories'));
    }

    #================ Validate Category and store category ============================
    public function store(CategoryRequest $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->save();

        return redirect()->route('Categories.index')->with('success', 'Category created successfully');
    }

    #================ Edit category ============================
    public function edit(Category $category)
    {
        $categories = Category::whereNull('parent_id')->get();

        return view('categories.edit', compact('category', 'categories'));
    }

    #================ Validate Category update and update category ============================
    public function update(CategoryRequest $request, Category $category)
    {
        $category->name = $request->input('name');
        $category->parent_id = $request->input('parent_id');
        $category->save();
        return redirect()->route('Categories.index')->with('success', 'Category update successfully');
     }

     #================ Delete category ============================
    public function destroy($id)
    {
      Category::find($id)->delete($id);
      return response()->json([
        'success' => true,
        'message' => 'Record Delete successfully!'
      ]);

    }

    #================ Category with nested list ============================
    public function cat_nested(){
        $categories = Category::with('sub_categories')->whereNull('parent_id')->get();

        return view('categories.nested', compact('categories'));
     }
}


