<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    #=========== redirect user as there role (admin/user)==================
    public function index()
    {
        if(Auth::user()->is_admin){
           $users = User::all();
           return view('admin_dashboard',compact('users'));

        }else{
            $user = Auth::user();
            return view('user_dashboard',compact('user'));

        }
    }


#=========== Show logged user details==================
    public function show(User $user){
        return view('user_dashboard',compact('user'));
    }
}
