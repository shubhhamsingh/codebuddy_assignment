<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

#=================== if user is admin then only admin can see user dashboard ==============
Route::get('/user/{user}',[App\Http\Controllers\HomeController::class,'show'])
    ->middleware('is_admin')
    ->name('user.show');

#=================== Auth user/admin can see category and nested listing of categories ==============
Route::group(['middleware' => 'auth'],function () {
    Route::get('/categories',[CategoryController::class,'index'])->name('Categories.index');
    Route::get('/nested-categories',[CategoryController::class,'cat_nested'])->name('Categories.nested');
    
});

#=================== only admin can create/edit/update and delete categories ==============
Route::group(['middleware' => ['auth','is_admin']],function(){
     Route::get('/create-category',[CategoryController::class,'create'])->name('Create.categories');
    Route::post('/create-category',[CategoryController::class,'store'])->name('Categories.store');
    Route::get('/edit-category/{category}',[CategoryController::class,'edit'])->name('Categories.edit');
    Route::post('/edit-category/{category}',[CategoryController::class,'update'])->name('Categories.update');
    Route::post('/delete-category/{category}',[CategoryController::class,'destroy'])->name('Categories.destroy');
});